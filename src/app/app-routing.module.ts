import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CBISFormComponent } from './components/cbis-form/cbis-form.component';
import { HomeComponent } from './components/home/home.component';
import { ListComponent } from './components/list/list.component';
import { LoginComponent } from './components/login/login.component';
import { QrCodeComponent } from './components/qr-code/qr-code.component';
import { ViewDetailsComponent } from './components/view-details/view-details.component';

const routes: Routes = [

  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'baleRegistration',
    component: CBISFormComponent
  },
  
 
  {
    path: 'list',
    component: ListComponent
  },
  {
    path: 'viewdetails',
    component: ViewDetailsComponent
  },
  {
    path: 'viewdetails/:uuid',
    component: ViewDetailsComponent
  },
  {
    path:'qrcode',
    component: QrCodeComponent
  },
  {
    path:'login',
    component: LoginComponent
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

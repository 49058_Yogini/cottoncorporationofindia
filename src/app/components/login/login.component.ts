import { Component } from '@angular/core';
import { loginService } from 'src/service/loginService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  credentials = {
    "username": '',
    "password": ''
  }
  constructor(private loginService:loginService) { }

  ngOnInit(): void {
  }
  onSubmit(){
    
    if((this.credentials.username!='' && this.credentials.password!='') && (this.credentials.username!=null && this.credentials.password!=null)){
      console.log("Ouput is submitted");
      this.loginService.generateToken(this.credentials).subscribe(
        (response:any) => {
          console.log(response.token);
          this.loginService.loginUser(response.token);
          
          window.location.href="/emigrantDashboard";
          alert("Welcome Emigrant "+this.credentials.username);
        },
        error => {
          console.log("getting errorrr")
          console.log(error);
        }

      )
      //generate token
    }else{
      console.log("values are empty")
    }
  }
}

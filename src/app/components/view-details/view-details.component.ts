import { Component, OnInit } from '@angular/core';
import { loginService } from 'src/service/loginService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-view-details',
  templateUrl: './view-details.component.html',
  styleUrls: ['./view-details.component.css']
})
export class ViewDetailsComponent implements OnInit {

  data: any;
  uuid:any ="";
  
  constructor(
    private loginService:loginService,
    private router :ActivatedRoute
    ) { }

  ngOnInit(): void {
    // throw new Error('Method not implemented.');
    this.router.params.subscribe(params=>{
      console.log("UUID in paramsssss ---",params['uuid']);
      this.uuid = params['uuid'];
      console.log("UUID in thi.uuidddddd ---",this.uuid);

    })
    this.getDataForUuidFromDb();
    console.log("after calling getDataForUuidFromDb");
  }
  onSubmit(){

  }
  getDataForUuidFromDb(){
  console.log("In getDataForUuidFromDb :--",this.uuid);
    this.loginService.getData(this.uuid).subscribe(res=>{
      this.data=res;
    })
    //console.log("In getDataForUuidFromDb data we got is --------",this.data);
  
  }

}

import { Component } from '@angular/core';
import { loginService } from 'src/service/loginService';
import { SharedService } from 'src/service/sharedService';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.component.html',
  styleUrls: ['./qr-code.component.css']
})
export class QrCodeComponent {
  dataToString:any="";
  
  constructor(
    private shared:SharedService ,
    private loginService:loginService  ){}

    ngOnInit(): void {
      this.dataToString = this.shared.getQrCodeUrl();
      // alert("inside onInit of QRCode"+this.dataToString)
    }
}

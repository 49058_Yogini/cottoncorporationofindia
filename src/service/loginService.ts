import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { list } from "src/app/models/list";

@Injectable({
    providedIn: 'root'
  })
export class loginService{
    uuid: any;
    uuid_response : any;
  dataToString:any='';

  constructor(private http:HttpClient) { }

  generateToken(credentials:any){
    //token generate
     return this.http.post ("http://10.210.9.65:8080/Bales/getbyusernameandpassword",credentials )

  }

  //login service
  loginUser(token:any){
    localStorage.setItem("token",token);
    return true;
  }

  isLoggedIn(){
    let token = localStorage.getItem("token");
    if(token ==undefined || token =='' || token ==null){
      return false;
    }else{
      return true
    }
  }

  saveData(data:any){
    return this.http.post("http://localhost:8080/Bales/save",data)
  }

  getData(value:String){
    const url = "http://localhost:8080/Bales/getbyuuid/"+value;
    return this.http.get(url);
  }

  getAllList(){
    return this.http.get<list[]>("http://localhost:8080/Bales/getalluuid")
  }

  setQrCodeUrl(uuid:any){
    this.dataToString = "http://10.210.9.65:4200/viewdetails/"+uuid
  }

  getQrCodeUrl(){
    return this.dataToString;
    }

    //for getting token
    getoken(){
        localStorage.getItem('token');
    }
    getUuid(){
        return this.uuid;
    
    }
    
    setUuid(uuid:any){
        console.log("In login service calling set uuid--", uuid);
        this.uuid = uuid;
        console.log("In login service calling set uuid and set--", this.uuid);
      }
}